﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CollectibleAcceptZone), true)]
public class CollectibleAcceptZoneDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CollectibleAcceptZone myScript = (CollectibleAcceptZone)target;
        if (GUILayout.Button("Activate"))
        {
            myScript.MockAccept();
        }

    }
}