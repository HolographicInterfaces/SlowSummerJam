﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameManager), true)]
public class GameManagerDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameManager myScript = (GameManager)target;
        if (GUILayout.Button("Show Dull"))
        {
            myScript.ApplyDullToAll();
        }

        if (GUILayout.Button("Show Normal"))
        {
            myScript.ApplyNormalToAll();
        }

        if (GUILayout.Button("Show Vibrant"))
        {
            myScript.ApplyVibrantToAll();
        }
    }
}