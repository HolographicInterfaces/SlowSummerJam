﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimation : MonoBehaviour {

    public float waitTime = 3;
    public float fadeSpeed = 2;
    private Text text;
    private float time;

    private Color startColor;

	// Use this for initialization
	void Start () {
        time = 0;
        text = GetComponent<Text>();
        startColor = text.material.color;
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time > waitTime && text.material.color != Color.clear)
        {
            text.material.color = Color.Lerp(text.material.color, Color.clear, fadeSpeed * Time.deltaTime);
        }
	}

    private void OnDestroy()
    {
        text.material.color = startColor;
    }
}
