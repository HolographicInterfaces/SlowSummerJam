﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableObject : MonoBehaviour {

    GameObject attachedTo;

    private void OnTriggerEnter(Collider other)
    {
        attachedTo = other.gameObject;
        this.transform.SetParent(other.transform);
        if (this.GetComponent<Rigidbody>())
            this.GetComponent<Rigidbody>().useGravity = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == attachedTo)
        {
            if (this.GetComponent<Rigidbody>())
                this.GetComponent<Rigidbody>().useGravity = true;
            attachedTo = null;
        }
    }
}
