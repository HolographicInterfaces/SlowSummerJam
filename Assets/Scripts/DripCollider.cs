﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DripCollider : MonoBehaviour {

    public AudioClip dripSound;

    AudioSource audioSource;

    private void Start()
    {
        audioSource = this.gameObject.AddComponent<AudioSource>();
        audioSource.clip = dripSound;
        audioSource.volume = .01f;

    }

    ParticleSystem ps;

    List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();

    void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
    }

    void OnParticleTrigger()
    {
        int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);

        for (int i = 0; i < numEnter; i++)
        {
            ParticleSystem.Particle p = enter[i];
            audioSource.Play();
            p.remainingLifetime = 0;
            enter[i] = p;
        }

        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
    }
}
