﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyColorControl : MonoBehaviour {



    public void ChangeColor()
    {
        Debug.Log("Change");

        Material levelMat = Resources.Load("skystartday", typeof(Material)) as Material;
        RenderSettings.skybox = levelMat;
        Debug.Log(RenderSettings.skybox);
        //StartCoroutine(BrightSky());
    }

    private void Start()
    {
        RenderSettings.skybox.SetFloat("_Blend", 0);
    }

    public static IEnumerator BrightSky()
    {
        float brightTime = 5;
        float elapsedTime = 0;

        while (elapsedTime < brightTime)
        {
            RenderSettings.skybox.SetFloat("_Blend", (elapsedTime / brightTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        RenderSettings.skybox.SetFloat("_Blend", 1);
    }
}
