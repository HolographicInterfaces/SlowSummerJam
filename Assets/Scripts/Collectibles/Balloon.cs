﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : Collectible
{
    public Canvas TextReveal;
    public Transform target;

    public override void ConfigureScene_Collected(CollectibleAcceptZone acceptZone)
    {
        base.ConfigureScene_Collected(acceptZone);
        RenderSettings.fogDensity *= .002f;
        SoundManager.instance.ActivateAudio(unlockSoundEnum);

        acceptZone.GetComponent<Light>().enabled = true;
        acceptZone.GetComponent<LensFlare>().enabled = true;
        acceptZone.GetComponent<SkyColorControl>().ChangeColor();
        TextReveal.enabled = true;
    }

    protected override void TriggerActivated()
    {
        if (!this.gameObject.activeSelf)
        {
            Detach();
            return;
        }
        base.TriggerActivated();
        Detach();
        if(primed)
        {
            StartCoroutine(RISE());
        } else
        {
            StartCoroutine(DelayedReturn());
        }
    }

    IEnumerator RISE()
    {
        if (this.GetComponent<Rigidbody>())
            this.GetComponent<Rigidbody>().isKinematic = true;

        float totalRiseTime = 20;
        Quaternion startingRotation = this.transform.rotation;
        while(totalRiseTime > 0)
        {
            this.transform.Translate((target.position - this.transform.position).normalized * Time.deltaTime * 15, Space.World);
            this.transform.rotation = Quaternion.Lerp(startingRotation, Quaternion.identity, (10 - totalRiseTime) / 10);
            totalRiseTime -= Time.deltaTime;
            if (totalRiseTime < 1)
            {
                Destroy(gameObject);
            }
            yield return null;
        }
        primed = false;
        StartCoroutine(DelayedReturn());
    }

    public override void ConfigureScene_NotCollected()
    {
        base.ConfigureScene_NotCollected();
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (attachedTo == null && other.name.Contains("Controller"))
            StopAllCoroutines();

        
        base.OnTriggerEnter(other);
        if (other.name == "BallonReleaseZone")
        {
            this.Prime();
        }
    }
}