﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bucket : Collectible
{
    public GameObject water;
    public Canvas TextReveal;

    public override void ConfigureScene_Collected(CollectibleAcceptZone acceptZone)
    {
        base.ConfigureScene_Collected(acceptZone);
        SoundManager.instance.ActivateAudio(unlockSoundEnum);
        TextReveal.enabled = true;
        foreach (Transform child in acceptZone.transform)
        {
            if(child.GetComponent<TheVapors>() != null)
            {
                child.gameObject.SetActive(true);
            }
        }
        Animator ami = acceptZone.GetComponentInChildren<Animator>();
        ami.Play("Base Stack");
    }

    protected override void TriggerActivated()
    {
        base.TriggerActivated();
        StartCoroutine(DelayedReturn());
        Detach();
    }

    public override void ConfigureScene_NotCollected()
    {
        base.ConfigureScene_NotCollected();
    }

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if(other.name == "FountainWater")
        {
            this.Prime();
            SteamVR_Controller.Input(1).TriggerHapticPulse(Convert.ToUInt16(interactionSound.length));
            water.SetActive(true);
        }
    }
}
