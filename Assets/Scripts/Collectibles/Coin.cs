﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectible
{
    public Canvas TextReveal;

    public override void ConfigureScene_Collected(CollectibleAcceptZone acceptZone)
    {
        Interaction();
        base.ConfigureScene_Collected(acceptZone);
        SoundManager.instance.ActivateAudio(unlockSoundEnum);
		acceptZone.GetComponent<SkyColorControl_MilkyColor>().ChangeColor();
        TextReveal.enabled = true;
    }

    protected override void TriggerActivated()
    {
        base.TriggerActivated();
        StartCoroutine(DelayedReturn());
        Detach();
    }

    public override void ConfigureScene_NotCollected()
    {
        base.ConfigureScene_NotCollected();
    }
}
