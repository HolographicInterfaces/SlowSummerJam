﻿using System;
using System.Collections;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public abstract class Collectible : MonoBehaviour
{
    public AvailableAudio unlockSoundEnum;
    public string unlockTagName;
    public bool canPickup = true;
    public bool primed = true;
    public bool isCollected;

    public float harmonyMaxDistance;

    public AudioClip harmonySound;
    public AudioClip collectionSound;
    public AudioClip interactionSound;

    Vector3 startingScale;
    Vector3 startingPosition;

    AudioSource audioSource;
    AudioSource interactionSoundSource;

    protected SteamVR_TrackedController attachedTo;

    private void Awake()
    {
        audioSource = this.GetComponent<AudioSource>();

        audioSource.clip = collectionSound;
        SoundManager.instance.AddAudioSource(audioSource);
    }

    public virtual void ConfigureScene_NotCollected()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag(unlockTagName))
        {
            FilteredObject fo = go.GetComponent<FilteredObject>();
            if (fo != null)
                fo.InitialConfig();
        }
    }

    public virtual void ConfigureScene_Collected(CollectibleAcceptZone acceptZone)
    {
        Detach();
        StopAllCoroutines();

        Debug.Log(this.name + " collected!");
        foreach (GameObject go in GameObject.FindGameObjectsWithTag(unlockTagName))
        {
            FilteredObject fo = go.GetComponent<FilteredObject>();
            if (fo != null)
                fo.Activate(1, .2f);
        }
        this.gameObject.SetActive(false);
    }

    protected virtual void TriggerActivated()
    {

    }

    protected virtual void Initialize()
    {
        this.startingScale = this.transform.localScale;
        this.startingPosition = this.transform.position;
    }

    protected IEnumerator DelayedReturn()
    {
        yield return new WaitForSeconds(10);
        this.transform.localScale = startingScale;
        this.transform.position = startingPosition;
    }

    public void SetHarmonyVolume(float volume)
    {
        audioSource.volume = volume;
    }

    public virtual void Prime()
    {
        primed = true;
        Interaction();
    }

    public virtual void Interaction()
    {
        if (interactionSound != null)
        {
            if (interactionSoundSource == null)
            {
                interactionSoundSource = this.transform.Find("InteractionAudio").GetComponent<AudioSource>();
            }

            interactionSoundSource.clip = interactionSound;
            interactionSoundSource.Play();
        }
    }

    private void Start()
    {
        Initialize();
        ConfigureScene_NotCollected();
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (canPickup)
        {
            if (other.name.Contains("Controller"))
            {
                this.transform.SetParent(other.transform);
                attachedTo = other.GetComponent<SteamVR_TrackedController>();
                attachedTo.TriggerClicked += Collectible_TriggerClicked;
                if (this.GetComponent<Rigidbody>())
                    this.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }

    private void Collectible_TriggerClicked(object sender, ClickedEventArgs e)
    {
        Detach();
        TriggerActivated();
    }

    protected void Detach()
    {
        this.transform.SetParent(null);
        if (attachedTo != null)
            attachedTo.TriggerClicked -= Collectible_TriggerClicked;
        attachedTo = null;
        if (this.GetComponent<Rigidbody>())
            this.GetComponent<Rigidbody>().isKinematic = false;
    }
}

