﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public GameObject playerRig;
    public GameObject playerEye;

    public List<Collectible> collectibles = new List<Collectible>();

    private void Awake()
    {
        if(playerRig == null)
        {
            playerRig = GameObject.Find("[CameraRig]");
        }
        if(playerEye ==null)
        {
            playerEye = GameObject.Find("Camera (eye)");
        }

        instance = this;
    }

    public void ApplyDullToAll()
    {
        foreach(FilteredObject fo in FindObjectsOfType<FilteredObject>())
        {
            fo.ApplyDull();
        }
    }

    public void ApplyNormalToAll()
    {
        foreach (FilteredObject fo in FindObjectsOfType<FilteredObject>())
        {
            fo.ApplyNormal();
        }
    }

    public void ApplyVibrantToAll()
    {
        foreach (FilteredObject fo in FindObjectsOfType<FilteredObject>())
        {
            fo.ApplyVibrant();
        }
    }
}
