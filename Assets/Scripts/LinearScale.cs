﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LinearScale : MonoBehaviour {

    public float minPosition;
    public float maxPosition;

    public AnimationCurve scaleCurve;

    public Transform trackedTransform;

    public AxisToUse axis;

    public bool constantUpdate;

    public enum AxisToUse
    {
        X,
        Y,
        Z
    }

    Vector3 scaleVar = new Vector3();

    private void Start() {
        if (trackedTransform == null)
            trackedTransform = this.transform;

        Scale();
        if (!constantUpdate)
            this.enabled = false;
    }

    // Update is called once per frame
    void Update () {
        Scale();
	}

    void Scale()
    {
        float value = 0;
        switch (axis)
        {
            case AxisToUse.X: value = trackedTransform.position.x; break;
            case AxisToUse.Y: value = trackedTransform.position.y / trackedTransform.lossyScale.y; break;
            case AxisToUse.Z: value = trackedTransform.position.z; break;
        }

        float ratio = Mathf.Clamp01((value - minPosition) / (maxPosition - minPosition));
        float scale = this.scaleCurve.Evaluate(ratio);

        scaleVar.x = scale;
        scaleVar.y = scale;
        scaleVar.z = scale;

        this.transform.localScale = scaleVar;
    }
}
