﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class TheVapors : MonoBehaviour {

    public float lifeTime;
    public float speed;
    public Transform target = null;

    [Range(0.0f, 1.0f)]
    public float randomness;

    /// <summary>
    /// Reduces the randomness over the lifetime
    /// </summary>
    public bool honeOverTime;

    float elapsedTime;
    Vector3 randomDir = new Vector3();
    Vector3 randomFactor = new Vector3();
    float startRandomness;
    bool ded = false;


	// Use this for initialization
	void Start () {
        randomFactor.x = ZO_To_NOO(Random.value);
        randomFactor.y = ZO_To_NOO(Random.value);
        randomFactor.z = ZO_To_NOO(Random.value);
        startRandomness = randomness;
        if(target == null)
        {
            //No transform, no problem, just don't expect it to go anywhere if you don't have the randomness up
            target = this.transform;
        }
        Destroy(this.gameObject, lifeTime*2);
    }
	
	// Update is called once per frame
	void Update () {

        if (ded)
            return;

        Vector3 targetDir = (target.position - this.transform.position).normalized;
        float compressedTime = Time.time * .1f;
        randomDir.x += ZO_To_NOO(Mathf.PerlinNoise(randomFactor.x * compressedTime, randomFactor.z * compressedTime))*2;
        randomDir.y += ZO_To_NOO(Mathf.PerlinNoise(randomFactor.y * compressedTime, randomFactor.x * compressedTime));
        randomDir.z += ZO_To_NOO(Mathf.PerlinNoise(randomFactor.z * compressedTime, randomFactor.y * compressedTime))*2;

        //TODO find the actual ground where we are and stay above that
        if (transform.position.y < 1)//Prefer to stay above ground
            randomDir.y = Mathf.Max(randomDir.y, 0);

        randomDir.Normalize();

        this.transform.Translate(Vector3.Lerp(targetDir, randomDir, randomness) *Time.deltaTime*speed);
        elapsedTime += Time.deltaTime;

        if(elapsedTime > lifeTime)
        {
            Die();
            return;
        }

        if (honeOverTime)
        {
            randomness = Mathf.Lerp(startRandomness, 0, elapsedTime / lifeTime);
        }
    }

    void Die()
    {
        this.GetComponent<ParticleSystem>().Stop();
        this.enabled = false;
        ded = true;
    }

    //Convert zero to one value to -1 to 1 value
    float ZO_To_NOO(float value)
    {
        return (value - .5f) * 2f;
    }
}
