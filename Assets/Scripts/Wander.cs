﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {
    public float speed = 5;
    public float directionChangeInterval = 10;
    public float maxHeadingChange = 30;

    public float heading = 0;
    public Vector3 targetRotation;

    //http://wiki.unity3d.com/index.php/Wander
    void Awake()
    {
        // Set random initial rotation
        transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);  // look in a random direction at start of frame.

        //StartCoroutine
        StartCoroutine(GetNewHeading());
    }

    void Update()
    {
        WatchoutForObsticals();
    }

    void WatchoutForObsticals()
    {
        Debug.DrawRay(transform.position, transform.forward * 2, Color.red);
        if (Physics.Raycast(transform.position, transform.forward, 2))
        {
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
            NewHeadingRoutine();
        }
        else
        {
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
            var forward = transform.TransformDirection(Vector3.forward);
            transform.position += forward * speed * Time.deltaTime;
        }
    }
 
    /// <summary>
    /// Repeatedly calculates a new direction to move towards.
    /// Use this instead of MonoBehaviour.InvokeRepeating so that the interval can be changed at runtime.
    /// </summary>
	IEnumerator GetNewHeading()
    {
        while (true)
        {
            NewHeadingRoutine();
            yield return new WaitForSeconds(directionChangeInterval);
        }
    }

    /// <summary>
    /// Calculates a new direction to move towards.
    /// </summary>
    void NewHeadingRoutine()
    {
        var floor = Mathf.Clamp(heading - maxHeadingChange, 0, 360);
        var ceil = Mathf.Clamp(heading + maxHeadingChange, 0, 360);
        heading = Random.Range(floor, ceil);
        targetRotation = new Vector3(0, heading, 0);
    }
}
