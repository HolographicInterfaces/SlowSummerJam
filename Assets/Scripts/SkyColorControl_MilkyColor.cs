﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyColorControl_MilkyColor : MonoBehaviour {
	
	public Material MorningSky;
	public Material MilkyGrey;
	public Material MilkyWay;
    public void ChangeColor()
    {
		var currentSky = RenderSettings.skybox;

		if (currentSky == MilkyGrey) {
            Debug.Log(RenderSettings.skybox);
            RenderSettings.skybox = MilkyWay;
            Debug.Log("MilkyWay!");
		} else if (currentSky == MorningSky) {
            RenderSettings.skybox = MorningSky;
            Debug.Log("Still morning!");
        } else {
            Debug.Log("Bugger off!");
            return;
		}

    }

    private void Start()
    {
        RenderSettings.skybox.SetFloat("_Blend", 0);
    }

    public static IEnumerator BrightSky()
    {
        float brightTime = 5;
        float elapsedTime = 0;

        while (elapsedTime < brightTime)
        {
            RenderSettings.skybox.SetFloat("_Blend", (elapsedTime / brightTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        RenderSettings.skybox.SetFloat("_Blend", 1);
    }
}