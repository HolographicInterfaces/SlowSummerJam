﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FacePlayer : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        var lookDir = (Camera.main.transform.position - transform.position).normalized;
        Vector3 lookScale = transform.rotation * -lookDir;
        this.transform.localScale = new Vector3(1 - Mathf.Abs(lookScale.x), 1 - Mathf.Abs(lookScale.y), 1 - Mathf.Abs(lookScale.z));
    }
}
