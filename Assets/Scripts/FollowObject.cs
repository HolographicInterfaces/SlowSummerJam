﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour {


    public Transform targetObject;
    public bool followPosition;
    public bool maintainPositionOffset;

    public bool followRotation;



    Vector3 positionOffset;
    Vector3 scaledOffset;

    private void Awake()
    {
        positionOffset = scaledOffset = this.transform.position - targetObject.position;
    }

    // Update is called once per frame
    void LateUpdate () {
		if(followPosition)
        {
            this.transform.position = targetObject.transform.position;
            if (maintainPositionOffset)
            {
                scaledOffset.x = positionOffset.x * targetObject.transform.lossyScale.x;
                scaledOffset.y = positionOffset.y * targetObject.transform.lossyScale.y;
                scaledOffset.z = positionOffset.z * targetObject.transform.lossyScale.z;
                transform.position += scaledOffset;
            }
        }

        if(followRotation)
        {
            transform.rotation = targetObject.rotation;
        }
	}
}
