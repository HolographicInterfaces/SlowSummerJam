﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider))]
public class GrowZone : MonoBehaviour {

    Vector3 minPosition;
    Vector3 maxPosition;

    public Transform trackedTransform = null;
    public Transform scaledTransform = null;

    bool scaleActive;

    BoxCollider colliderZone;

    public float enterScale;
    public float exitScale;

    public GrowZone NextZone;

    private void Start()
    {
        colliderZone = GetComponent<BoxCollider>();

        maxPosition = colliderZone.transform.position + (colliderZone.transform.forward * colliderZone.bounds.extents.z);
        minPosition = colliderZone.transform.position - (colliderZone.transform.forward * colliderZone.bounds.extents.z);
        minPosition.y = 0;
        if (trackedTransform == null)
        {
            trackedTransform = GameManager.instance.playerEye.transform;
            Debug.Log("You should set tracked transform!");
        }

        if(scaledTransform == null)
        {
            trackedTransform = GameManager.instance.playerRig.transform;
            Debug.Log("You should set scaled transform!");
        }

        if(NextZone != null)
        {
            if(NextZone.enterScale != this.exitScale)
            {
                Debug.Log("Scale mismatch!");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.transform == trackedTransform)
        {
            Scale();
            scaleActive = false;
            StartCoroutine(DelayedScale());

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform == trackedTransform)
        {
            scaleActive = true;
            StartCoroutine(ConstantScale());
        }
    }

    private IEnumerator DelayedScale()
    {
        yield return null;
        Scale();
        StopAllCoroutines();
    }

    private IEnumerator ConstantScale()
    {
        while(scaleActive)
        {
            Scale();
            yield return null;
        }
    }

    private void Scale()
    {
        Vector3 trackedFloor = Math3d.ProjectPointOnLineSegment(minPosition, maxPosition, trackedTransform.position);

        float toMin = (minPosition - trackedFloor).magnitude;
        float endToEnd = colliderZone.bounds.size.z;

        float ratio = Mathf.Clamp01(toMin / endToEnd);
        float scale = enterScale + (ratio * (exitScale - enterScale));

        scaledTransform.transform.localScale = Vector3.one * scale;
    }

    private void OnDrawGizmos()
    {
        Vector3 trackedFloor = Math3d.ProjectPointOnLineSegment(minPosition, maxPosition, trackedTransform.position);

        Gizmos.DrawWireCube(trackedFloor, Vector3.one * 2);
    }
}
