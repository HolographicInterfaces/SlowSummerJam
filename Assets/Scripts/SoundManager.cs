﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;
    public List<AudioSource> audioSource;
    private List<AudioSource> playerAudio;

    private void Awake()
    {
        playerAudio = new List<AudioSource>();
        audioSource = new List<AudioSource>();
        instance = this;
    }

    // Use this for initialization
    void Start () {
        TakeAction();
    }

    void TakeAction()
    {
        for (var index = 0; index < audioSource.Count; index++)
        {
            var pA = gameObject.AddComponent<AudioSource>();
            playerAudio.Add(pA);
            playerAudio[index].clip = audioSource[index].clip;
          //playerAudio[index].enabled = true;
            playerAudio[index].Play();
            playerAudio[index].loop = true;
            playerAudio[index].mute = true;
            Debug.Log("Audio muted?");
        }
    }

    public void ActivateAudio(AvailableAudio trackName)
    {
        var index = playerAudio.FindIndex(a => a.clip.name == trackName.ToString());
        if(index > -1)
        {
            playerAudio[index].mute = false;
        }
    }

    public List<AudioSource> AddAudioSource(AudioSource source)
    {
        //Debug.Log("Audisource defined as " + audioSource);
        audioSource.Add(source);
               return audioSource;
    }
}

public enum AvailableAudio
{
    GardenUnlock,
    SunUnlock,
    TempleUnlock
}
