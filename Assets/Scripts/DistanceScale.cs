﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceScale : MonoBehaviour {

    public float minDistance;
    public float maxDistance;

    public AnimationCurve scaleCurve;

    public Vector3 origin;
    public Transform trackedTransform;

    public bool constantUpdate;

    Vector3 scaleVar = new Vector3();

    private void Start()
    {
        if (trackedTransform == null)
            trackedTransform = this.transform;

        Scale();
        if (!constantUpdate)
            this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        Scale();
    }

    void Scale()
    {
        float value = Vector3.Distance(origin, trackedTransform.position);

        float ratio = Mathf.Clamp01((value - minDistance) / (maxDistance - minDistance));
        float scale = this.scaleCurve.Evaluate(ratio);

        scaleVar.x = scale;
        scaleVar.y = scale;
        scaleVar.z = scale;

        trackedTransform.transform.localScale = scaleVar;
    }
}
