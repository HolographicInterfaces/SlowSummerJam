﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(MeshRenderer))]
public class FilteredObject : MonoBehaviour {
    public Material dullMaterial;
    public Material normalMaterial;
    public Material vibrantMaterial;

   // public Shader dullShader;
    public Shader normalShader;

    MeshRenderer meshRenderer;
    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        dullMaterial.shader = normalShader;
        normalMaterial.shader = normalShader;
        vibrantMaterial.shader = normalShader;
    }

    public void InitialConfig()
    {
        ApplyDull();
    }

    public void Activate(float timeToVibrant, float timeBackToNormal)
    {
        StartCoroutine(ActivateDullVibrantNormal(timeToVibrant, timeBackToNormal));
    }

    public void ApplyDull()
    {
        dullMaterial.shader = normalShader;
        GetComponent<MeshRenderer>().sharedMaterials = Enumerable.Repeat(dullMaterial, GetComponent<MeshRenderer>().sharedMaterials.Length).ToArray();
    }

    public void ApplyNormal()
    {
        normalMaterial.shader = normalShader;
        GetComponent<MeshRenderer>().sharedMaterials = Enumerable.Repeat(normalMaterial, GetComponent<MeshRenderer>().sharedMaterials.Length).ToArray();
    }

    public void ApplyVibrant()
    {
        vibrantMaterial.shader = normalShader;
        GetComponent<MeshRenderer>().sharedMaterials = Enumerable.Repeat(vibrantMaterial, GetComponent<MeshRenderer>().sharedMaterials.Length).ToArray();
    }

    public IEnumerator ActivateDullVibrantNormal(float timeToVibrant, float timeBackToNormal)
    {
        float timeElapsed = 0;
        MeshRenderer dullMeshRenderer = GetComponent<MeshRenderer>();

        while (timeElapsed < timeToVibrant)
        {
            timeElapsed += Time.deltaTime;

            for (int m = 0; m < dullMeshRenderer.materials.Length; m++)
            {
                dullMeshRenderer.materials[m].Lerp(dullMaterial, vibrantMaterial, timeElapsed / timeToVibrant);
            }
            yield return null;
        }

        timeElapsed = 0;
        while (timeElapsed < timeBackToNormal)
        {
            timeElapsed += Time.deltaTime;

            for (int m = 0; m < dullMeshRenderer.materials.Length; m++)
            {
                dullMeshRenderer.materials[m].Lerp(vibrantMaterial, normalMaterial, timeElapsed / timeBackToNormal);
            }
            yield return null;
        }

        dullMeshRenderer.sharedMaterials = Enumerable.Repeat(normalMaterial, dullMeshRenderer.sharedMaterials.Length).ToArray();
    }
}
