﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
public class CollectibleAcceptZone : MonoBehaviour {

    public Collectible acceptedCollectible;

    AudioSource audioSource;

    public void Start()
    {
        if(acceptedCollectible == null)
        {
            Debug.Log("Collection zone without collectible");
            this.enabled = false;
        }
        audioSource = this.GetComponent<AudioSource>();
    }

    public void OnTriggerEnter(Collider other)
    {
        Activate(other.GetComponent<Collectible>());
    }

    private void Update()
    {
        float distance = Vector3.Distance(this.transform.position, acceptedCollectible.transform.position);
        if(distance < acceptedCollectible.harmonyMaxDistance)
        {
            audioSource.clip = acceptedCollectible.harmonySound;
            audioSource.volume = (distance/ acceptedCollectible.harmonyMaxDistance);
            acceptedCollectible.SetHarmonyVolume(1 - audioSource.volume);
        }
    }

    void Activate(Collectible collectible)
    {
        if (collectible != null)
        {
            if (acceptedCollectible != collectible)
            {
                //TODO Reset collectible?
                return;
            }
            if (collectible.primed)
            {
                collectible.ConfigureScene_Collected(this);
                ParticleSystem[] ps = GetComponentsInChildren<ParticleSystem>();
                foreach(ParticleSystem p in ps)
                {
                    p.Play();
                }
            }
            else
            {
                //TODO play sound or indicate not primed collectible 
            }
        }
    }

    public void MockAccept()
    {
       
        acceptedCollectible.primed = true;
        Activate(acceptedCollectible);
    }
}
